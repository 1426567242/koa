
const Koa = require('koa');
const Router = require('koa-router'); 
const app = new Koa();
const router = new Router();

var logger = require('koa-logger')
  , json = require('koa-json')
  , views = require('koa-views')
  , onerror = require('koa-onerror');

var index = require('./routes/index');
var users = require('./routes/users');


// global middlewares
app.use(views('views', {
  root: __dirname + '/views',
  default: 'jade'
}));

app.use(require('koa-bodyparser')());
app.use(json());
app.use(logger());


app.use(function *(next){
  var start = new Date;
  yield next;
  var ms = new Date - start;
  console.log('%s %s - %s', this.method, this.url, ms);
});

app.use(require('koa-static')(__dirname + '/public'));

// routes definition
app.use(index.routes(),index.allowedMethods());
app.use(users.routes(),users.allowedMethods());


app.listen(3000, () => {
  console.log('server started at http://127.0.0.1:3000/');
});
module.exports = app;
